from diffusers.pipelines.stable_diffusion_xl.pipeline_stable_diffusion_xl import StableDiffusionXLPipeline
import numpy as np
import base64

from typing import Protocol
from PIL.Image import Image, fromarray as image_from_array
from io import BytesIO

import home_image_generator.config as config


class ImageGeneratorProtocol(Protocol):
    def generate(self, prompt: str) -> str:
        """Generate an image from a prompt.

        Args:
            prompt (str): The prompt to generate an image from.

        Returns:
            str: The base64 encoded image."""
        ...


class ImageGenerator(ImageGeneratorProtocol):
    pipeline: StableDiffusionXLPipeline

    def __init__(self) -> None:
        from diffusers.pipelines.auto_pipeline import AutoPipelineForText2Image
        import torch

        has_cuda = torch.cuda.is_available()

        if not has_cuda and not config.ALLOW_CPU:
            raise RuntimeError("No GPU available and CPU is not allowed.")

        pipeline = AutoPipelineForText2Image.from_pretrained(
            "stabilityai/sdxl-turbo",
            torch_dtype=torch.float16,
            variant="fp16",
        )
        pipeline.to("cuda" if has_cuda else "cpu")

        self.pipeline = pipeline # type: ignore

    def generate(self, prompt: str) -> str:
        image: Image = self.pipeline(prompt=prompt, num_inference_steps=1, guidance_scale=0.0).images[0] # type: ignore

        buffer = BytesIO()
        image.save(buffer, format="PNG")

        return base64.b64encode(buffer.getvalue()).decode("utf-8")


class TestImageGenerator(ImageGeneratorProtocol):
    def generate(self, _: str) -> str:
        image_data = np.zeros((512, 512, 3), dtype=np.uint8)

        # randomize color
        color = np.random.randint(0, 255, 3, dtype=np.uint8)
        image_data[:] = color

        image_bytes = image_from_array(image_data)

        buffer = BytesIO()
        image_bytes.save(buffer, format="PNG")

        return base64.b64encode(buffer.getvalue()).decode("utf-8")
