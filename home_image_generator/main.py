import uvicorn
import click

import home_image_generator.config as config
from home_image_generator.starlette.app import build_app


@click.command()
@click.option("--port", default=8000, help="Port to run the server on.")
@click.option("--host", default="127.0.0.1", help="Host to run the server on.")
@click.option("--debug", default=config.DEBUG, is_flag=True, help="Run in debug mode.")
@click.option(
    "--use-random-image-generator",
    default=config.RANDOM_IMAGE_GENERATOR,
    is_flag=True,
    help="Use debug image generator for random colored images.",
)
@click.option(
    "--allow-cpu",
    default=config.ALLOW_CPU,
    is_flag=True,
    help="Allow CPU for image generation.",
)
def main(
    port: int, host: str, debug: bool, use_random_image_generator: bool, allow_cpu: bool
):
    config.DEBUG = config.DEBUG or debug
    config.RANDOM_IMAGE_GENERATOR = (
        config.RANDOM_IMAGE_GENERATOR or use_random_image_generator
    )
    config.ALLOW_CPU = config.ALLOW_CPU or allow_cpu

    uvicorn.run(build_app(debug, use_random_image_generator), host=host, port=port)


if __name__ == "__main__":
    main()
