from starlette.requests import Request
from starlette.responses import HTMLResponse
import jinja2

jinja_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader("public"), autoescape=jinja2.select_autoescape()
)


async def index_endpoint(request: Request) -> HTMLResponse:
    name = request.path_params.get("name", "world")
    template = jinja_env.get_template("index.html")
    return HTMLResponse(template.render(name=name))
