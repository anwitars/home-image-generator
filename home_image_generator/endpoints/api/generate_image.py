from starlette.requests import Request
from starlette.responses import PlainTextResponse

from home_image_generator.image_generator import ImageGeneratorProtocol


async def generate_image_endpoint(req: Request) -> PlainTextResponse:
    image_generator: ImageGeneratorProtocol = req.app.state.image_generator
    prompt = req.query_params.get("prompt", "") # for now, empty prompt is okay

    return PlainTextResponse(image_generator.generate(prompt), media_type="text/plain")
