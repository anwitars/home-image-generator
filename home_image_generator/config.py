from os import environ


DEBUG = environ.get("HIG_DEBUG", "0") == "1"
ALLOW_CPU = environ.get("HIG_ALLOW_CPU", "0") == "1"
RANDOM_IMAGE_GENERATOR = environ.get("HIG_USE_RANDOM_IMAGE_GENERATOR", "0") == "1"
