from starlette.applications import Starlette
from home_image_generator.image_generator import TestImageGenerator, ImageGenerator
from home_image_generator.starlette.routes import routes

def build_on_startup(use_random_image_generator: bool):
    def startup():
        app.state.image_generator = TestImageGenerator() if use_random_image_generator else ImageGenerator()

    return startup

app: Starlette = None # type: ignore | This variable is initialized by the cli app using the build_app function

def build_app(debug: bool, use_random_image_generator: bool):
    global app
    app = Starlette(debug=debug, routes=routes, on_startup=[build_on_startup(use_random_image_generator)])
    return app
