from starlette.routing import Route, Mount
from starlette.staticfiles import StaticFiles

from home_image_generator.endpoints.api.generate_image import generate_image_endpoint
from home_image_generator.endpoints.index import index_endpoint

routes = [
    Mount("/static", app=StaticFiles(directory="static"), name="static"),
    Route("/api/generate-image", generate_image_endpoint, methods=["GET"]),
    Route("/", index_endpoint, methods=["GET"]),
]
